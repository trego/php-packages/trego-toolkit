<?php

namespace Trego\Toolkit;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;

class HttpClient
{
    /**
     * @var Client
     */
    protected $client;

    public function __construct()
    {
        $this->client = new Client;
    }

    protected function getDefaultHeaders()
    {
        return [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ];
    }

    /**
     * Creates and performs request.
     *
     * @param string $method
     * @param string $url
     * @param array $data
     * @param array $headers
     * @return array
     */
    public function request($method, $url, $data = [], $headers = [])
    {
        switch (strtolower($method)) {
            case 'get':
                return $this->get($url, $headers);
            case 'post':
                return $this->post($url, $data, $headers);
            case 'patch':
                return $this->patch($url, $data, $headers);
            case 'delete':
                return $this->delete($url, $headers);
        }
    }

    protected function get($url, $headers = [])
    {
        try {
            $response = $this->client->request('GET', $url, [
                'headers' => array_merge($this->getDefaultHeaders(), $headers),
            ]);

            return json_decode($response->getBody(), true);
        } catch (RequestException $e) {
            throw $e;
        }
    }

    protected function post($url, $data, $headers = [])
    {
        try {
            $response = $this->client->request('POST', $url, [
                RequestOptions::JSON => $data,
                'headers' => array_merge($this->getDefaultHeaders(), $headers),
            ]);

            return json_decode($response->getBody(), true);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    protected function patch($url, $data, $headers = [])
    {
        try {
            $response = $this->client->request('PATCH', $url, [
                RequestOptions::JSON => $data,
                'headers' => array_merge($this->getDefaultHeaders(), $headers),
            ]);

            return json_decode($response->getBody(), true);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    protected function delete($url, $headers = [])
    {
        try {
            $response = $this->client->request('DELETE', $url, [
                'headers' => array_merge($this->getDefaultHeaders(), $headers),
            ]);

            return json_decode($response->getBody(), true);
        } catch (RequestException $e) {
            throw $e;
        }
    }
}
