<?php

namespace Trego\Toolkit\Test;

use PHPUnit\Framework\TestCase;
use Trego\Toolkit\HttpClient;

class HttpClientTest extends TestCase
{
    protected const TEST_URL = 'http://ec2-3-0-55-54.ap-southeast-1.compute.amazonaws.com';

    protected $client;

    public function setUp(): void
    {
        parent::setUp();

        $this->client = new HttpClient();
    }

    public function testInit()
    {
        $this->assertTrue(true);
    }

    public function testGetRequest()
    {
        $response = $this->client->request('get', self::TEST_URL);

        $this->assertTrue(is_array($response));
    }

    public function testUsersGetRequest()
    {
        $response = $this->client->request('get', self::TEST_URL . '/users');
        $this->assertTrue(is_array($response));
    }

    public function testPostRequest()
    {
        $response = $this->client->request('POST', self::TEST_URL . '/roles', [
            'name' => 'Test_Role_1',
        ]);

        $this->assertTrue($response['data']['name'] === 'Test_Role_1');
    }

    public function testPatchRequest()
    {
        $response = $this->client->request('PATCH', self::TEST_URL . '/roles/4', [
            'name' => 'Test_Role_1_updated',
        ]);

        $this->assertTrue($response['data']['name'] === 'Test_Role_1_updated');
    }
    
    public function testRolesDeleteRequest()
    {
        $roles = $this->client->request('delete', self::TEST_URL . '/roles/3');

        $find_role = $this->client->request('get', self::TEST_URL, '/roles/3');
    
        $this->assertNull($find_role);
    }
}
