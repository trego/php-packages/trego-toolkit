<?php

namespace Trego\Toolkit\Aws;

use Aws\Credentials\Credentials;
use Aws\Sqs\SqsClient;
use Aws\Sdk;

class AwsConnector
{
    /**
     * @var Sdk
     */
    protected $aws;

    public function __construct($key, $secret)
    {
        $this->aws = new Sdk([
            'region'   => 'ap-southeast-1',
            'version'  => 'latest',
            'credentials' => new Credentials($key, $secret),
        ]);
    }

    /**
     * Create a service.
     *
     * @param string $service
     * @return mixed
     */
    public function create($service)
    {
        switch ($service) {
            case 'dynamoDb':
                return $this->aws->createDynamoDb();
            case 'sqs':
                return $this->aws->createSqs();
            default:
                return null;
        }
    }
}
